import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  private BASE_URL = "https://rawgit.com/rohit-dantas-conichi";
  
  private http = inject(HttpClient);

  getGeneralInformations(): Observable<any> {
    return <Observable<any>> this.http.get(`${this.BASE_URL}/3cf7d1da56f60959adfc4664cff31189/raw/e97db2ad3940dce2a7da1ececcc1375b1f46311d/page-select_billing_address-en.json`);
  }

  getAddresses(): Observable<any> {
    return <Observable<any>> this.http.get(`${this.BASE_URL}/48635f6c8eead55a4b16e8670a813ac5/raw/eefc1c5180234519816df4d7293892fb92bab0e0/organism-billing_addresses-en.json`);
  }
}
